mod save_data;
use save_data::file_exists;
use serde::{Deserialize, Serialize};
use serenity::{async_trait, model::{channel::Message, gateway::Ready, id::GuildId}, prelude::*};

  struct Handler;

  #[async_trait]
  impl EventHandler for Handler {
    async fn message(&self, context: Context, msg: Message) {
      if msg.content == "lets go".to_string(){
        if let Err(why) = msg.channel_id.say(&context.http, "lessss gooooo").await {
            println!("Error sending message: {:?}", why);
        }
      }else if msg.content.contains("!set-vc") {
            let params: Vec<&str> = msg.content.split(" ").collect();
            msg.channel_id.say(&context.http, format!("vc set to {}!", params[1])).await.unwrap();
            if save_data::file_exists(&format!("{}.json", msg.guild_id.unwrap())) {
                println!("Loading server {} config...", msg.guild_id.unwrap());
            }else {
                let data = ServerData{guild_id: msg.guild_id.unwrap(), vc_id: params[1].parse::<i64>().unwrap()};
                if !file_exists("guild_data") {
                  save_data::create_dir("guild_data");
                }
                save_data::write_data(&serde_json::to_string(&data).unwrap(), &format!("guild_data/{}.json", data.guild_id)).unwrap();
                println!("New guild config written successfully!");
            }
      }
    }
  }
  #[tokio::main]
  async fn main() {
    println!("loading config...");
    let config: ConfigData;
    if save_data::file_exists("config.json") {
        config = serde_json::from_str(&save_data::load_to_string("config.json")).unwrap();
        println!("Config load success! Bot started!");
        let mut client = Client::builder(config.token.to_string()).event_handler(Handler).await.expect("Err creating client");
        if let Err(why) = client.start().await {
            println!("Client error: {:?}", why);
    }
    }else {
        println!("ERROR: Config not found! please exit this program and fill in the config!");
        config = ConfigData{token: "Please insert bot token here".to_string()};
        save_data::write_data(&serde_json::to_string(&config).unwrap(), "config.json").unwrap();
    }
  }
  #[derive(Serialize, Deserialize, Clone)]
  struct ConfigData {
      token: String,
  }
  #[derive(Serialize, Deserialize, Clone)]
  struct ServerData {
      guild_id: GuildId,
      vc_id: i64
  }