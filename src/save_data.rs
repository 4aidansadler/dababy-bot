use std::fs::*;
use std::fs;
use std::io::prelude::*;
use std::path::Path;

pub fn write_data(data: &str, file_name: &str) -> std::io::Result<String> {
    let mut file = File::create(file_name)?;
    file.write_all(format!("{}", data).as_bytes())?;
    Ok(String::from("Data save success!"))
}

pub fn load_to_string(path: &str) -> String{
    fs::read_to_string(path).unwrap()
}

pub fn file_exists(path: &str) -> bool {
    if Path::new(&path).exists() {
        return true;
    }else {
        return false;
    }
}

pub fn create_dir(path: &str) {
    fs::create_dir(path).unwrap();
}